"""CLI module."""

import sys


def main() -> int:
    """Console entry point."""


if __name__ == '__main__':
    sys.exit(main())
