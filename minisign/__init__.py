"""A dead simple tool to sign files and verify signatures.

Minisign is portable, lightweight, and uses the highly secure Ed25519 public-key
signature system. It is compatible with OpenBSD signify for public keys and
signatures.

Minisign is made by Frank Denis and licensed under ISC. This Python port is made
by HacKan and licensed under MPL v2.0.

License:
   Copyright (C) 2020 HacKan (https://hackan.net)
   This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at http://mozilla.org/MPL/2.0/.
"""

__version__ = '0.1.0+0.9'
